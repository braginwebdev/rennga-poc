import * as React from 'react';
import { connect } from 'react-redux';
import { SubApp as Flights } from 'rennga-poc-results';
import { AppState } from '../../store';
import { IFlight } from '../../store/search-bar/types';

class Results extends React.Component<IFlight> {
  public render() {
    const { departurePoint, arrivalPoint, departureDate, arrivalDate } = this.props;
    return (
      <section className="search-results">
        <Flights
          searchData={{
            departurePoint,
            arrivalPoint,
            departureDate,
            arrivalDate,
          }}
        />
      </section>
    );
  }
}

const mapStateToProps = (state: AppState): IFlight => ({
  departurePoint: state.searchBar.flightPayload.departurePoint,
  arrivalPoint: state.searchBar.flightPayload.arrivalPoint,
  departureDate: state.searchBar.flightPayload.departureDate,
  arrivalDate: state.searchBar.flightPayload.arrivalDate,
});

export default connect(mapStateToProps)(Results);
