import { Moment } from 'moment';
import * as React from 'react';
import { SyntheticEvent } from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import FlightSearchBar from '../../components/home/FlightSearchBar';
import { AppState } from '../../store';
import { searchFlights, searchHotels } from '../../store/search-bar/actions';
import { IFlight, IHotel, ISearchBarState, SearchTypes } from '../../store/search-bar/types';

interface ISearchBarProps {
  searchBar: ISearchBarState;
  searchFlights: typeof searchFlights;
  searchHotels: typeof searchHotels;
}

class SearchBar extends React.Component<ISearchBarProps, ISearchBarState> {
  constructor(props: Readonly<ISearchBarProps>) {
    super(props);
    this.state = this.props.searchBar;
  }

  public render() {
    let searchBar = null;
    switch (this.state.searchType) {
      case SearchTypes.flight:
        searchBar = (
          <FlightSearchBar
            changePoint={this.changePoint}
            onDatesChange={this.onDatesChange}
            onDatesFocusChange={this.onDatesFocusChange}
            focusedDateInput={this.state.focusedDateInput}
            flightPayload={this.state.flightPayload}
          />
        );
        break;
      case SearchTypes.hotel:
        searchBar = <div />;
        break;
    }

    return (
      <section className="search-bar">
        {searchBar}
        <button onClick={this.search}>Search</button>
      </section>
    );
  }
  private changePoint = (event: SyntheticEvent<HTMLSelectElement, Event>) => {
    const target = event.target as HTMLTextAreaElement;
    switch (this.state.searchType) {
      case SearchTypes.flight:
        this.setState(prevState => {
          const state = { ...prevState };
          state.flightPayload[target.name] = target.value;
          return state;
        });
        break;
      case SearchTypes.hotel:
        this.setState(prevState => {
          const state = { ...prevState };
          state.hotelPayload[target.name] = target.value;
          return state;
        });
        break;
    }
  };
  private onDatesChange = ({ startDate, endDate }: { startDate: Moment; endDate: Moment }) => {
    switch (this.state.searchType) {
      case SearchTypes.flight:
        this.setState(prevState => {
          const state = { ...prevState };
          state.flightPayload.departureDate = startDate;
          state.flightPayload.arrivalDate = endDate;
          return state;
        });
        break;
      case SearchTypes.hotel:
        this.setState(prevState => {
          const state = { ...prevState };
          state.hotelPayload.departureDate = startDate;
          state.hotelPayload.arrivalDate = endDate;
          return state;
        });
        break;
    }
  };
  private onDatesFocusChange = (focusedDateInput: any) => {
    this.setState({ focusedDateInput });
  };
  private search = () => {
    switch (this.state.searchType) {
      case SearchTypes.flight:
        this.props.searchFlights(this.state.flightPayload);
        break;
      case SearchTypes.hotel:
        this.props.searchHotels(this.state.hotelPayload);
        break;
    }
  };
}

const mapStateToProps = (state: AppState) => ({
  searchBar: state.searchBar,
});

const dispatchStateToProps = (dispatch: Dispatch) => ({
  searchFlights: (payload: IFlight) => dispatch(searchFlights(payload)),
  searchHotels: (payload: IHotel) => dispatch(searchHotels(payload)),
});

export default connect(
  mapStateToProps,
  dispatchStateToProps
)(SearchBar);
