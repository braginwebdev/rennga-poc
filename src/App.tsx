import * as React from 'react';

import { BrowserRouter as Router, Route } from 'react-router-dom';

import HomePage from './components/home/HomePage';

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <Router>
          <Route path="/" component={HomePage} exact={true} />
        </Router>
      </div>
    );
  }
}

export default App;
