import * as React from 'react';
import Results from '../../containers/home/Results';
import SearchBar from '../../containers/home/SearchBar';

const HomePage = () => (
  <section>
    <SearchBar />
    <Results />
  </section>
);

export default HomePage;
