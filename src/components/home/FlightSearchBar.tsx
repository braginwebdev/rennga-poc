import * as React from 'react';
import { DateRangePicker } from 'react-dates';
import { cities } from '../../data/cities';

const FlightSearchBar = (props: any) => (
  <div>
    <select
      name="departurePoint"
      value={props.flightPayload.departurePoint}
      onChange={props.changePoint}>
      {Object.values(cities).map((city, key) => (
        <option key={key} value={city}>
          {city}
        </option>
      ))}
    </select>
    <select
      name="arrivalPoint"
      value={props.flightPayload.arrivalPoint}
      onChange={props.changePoint}>
      {Object.values(cities).map((city, key) => (
        <option key={key} value={city}>
          {city}
        </option>
      ))}
    </select>
    <DateRangePicker
      startDate={props.flightPayload.departureDate}
      endDate={props.flightPayload.arrivalDate}
      startDateId="departureDate"
      endDateId="arrivalDate"
      focusedInput={props.focusedDateInput}
      onDatesChange={props.onDatesChange}
      onFocusChange={props.onDatesFocusChange}
    />
  </div>
);

export default FlightSearchBar;
