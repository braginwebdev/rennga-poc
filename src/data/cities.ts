export const cities = {
  tokyo: 'Tokyo',
  yokohama: 'Yokohama',
  osaka: 'Osaka',
  nagoya: 'Nagoya',
  sapporo: 'Sapporo',
  fukuoka: 'Fukuoka',
};
