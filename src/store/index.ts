import { combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { searchFlightsReducer } from './search-bar/reducers';

const rootReducer = combineReducers({
  searchBar: searchFlightsReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

export default function configureStore() {
  return createStore(rootReducer, composeWithDevTools());
}
