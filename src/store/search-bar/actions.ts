import { IFlight, IHotel, SEARCH_FLIGHTS, SEARCH_HOTELS, SearchActionTypes } from './types';

export function searchFlights(payload: IFlight): SearchActionTypes {
  return {
    type: SEARCH_FLIGHTS,
    payload,
  };
}

export function searchHotels(payload: IHotel): SearchActionTypes {
  return {
    type: SEARCH_HOTELS,
    payload,
  };
}
