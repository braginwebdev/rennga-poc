import * as moment from 'moment';
import {
  ISearchBarState,
  SEARCH_FLIGHTS,
  SEARCH_HOTELS,
  SearchActionTypes,
  SearchTypes,
} from './types';

const initialState: ISearchBarState = {
  searchType: SearchTypes.flight,
  flightPayload: {
    departurePoint: 'Tokyo',
    arrivalPoint: 'Yokohama',
    departureDate: moment(),
    arrivalDate: moment().add(1, 'd'),
  },
  hotelPayload: {
    departureDate: moment(),
    arrivalDate: moment().add(7, 'd'),
  },
  focusedDateInput: null,
};

export function searchFlightsReducer(
  state = initialState,
  action: SearchActionTypes
): ISearchBarState {
  switch (action.type) {
    case SEARCH_FLIGHTS: {
      const newState = { ...state };
      newState.flightPayload = action.payload;
      return newState;
    }
    case SEARCH_HOTELS: {
      const newState = { ...state };
      newState.hotelPayload = action.payload;
      return newState;
    }
    default:
      return state;
  }
}
