import { Moment } from 'moment';

export const SEARCH_FLIGHTS = 'SEARCH_FLIGHTS';
export const SEARCH_HOTELS = 'SEARCH_HOTELS';

export enum SearchTypes {
  flight = 'flight',
  hotel = 'hotel',
}

export interface IFlight {
  departurePoint: string;
  arrivalPoint: string;
  departureDate: Moment;
  arrivalDate: Moment;
}

export interface IHotel {
  departureDate: Moment;
  arrivalDate: Moment;
}

export interface ISearchBarState {
  searchType: SearchTypes;
  flightPayload: IFlight;
  hotelPayload: IHotel;
  focusedDateInput: 'startDate' | 'endDate' | null;
}

interface ISearchFlightsAction {
  type: typeof SEARCH_FLIGHTS;
  payload: IFlight;
}

interface ISearchHotelsAction {
  type: typeof SEARCH_HOTELS;
  payload: IHotel;
}

export type SearchActionTypes = ISearchFlightsAction | ISearchHotelsAction;
